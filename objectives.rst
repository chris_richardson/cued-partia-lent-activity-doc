Learning objectives
===================

.. topic:: Programming skills

   - Reinforcement of skills developed in Michaelmas Term.
   - Introduction to multi-file library implementations.
   - Working with user-defined objects.

.. topic:: Development skills

   - Skills for working in teams.
   - Designing a working library for specific tasks.
   - Working to a realistic project specification.
   - Effective use of version control.
   - Devising tests.
