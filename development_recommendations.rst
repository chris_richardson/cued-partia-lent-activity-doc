Development recommendations
===========================

Development environment
-----------------------

.. todo::

   Recommended environment, editor, etc.


Working in a team
-----------------

Most software is developed in teams, and working effectively in a team
developing a piece of software requires certain skills and practices.
At a planning level:

- Examine the required task, then discuss and decide on the
  dependencies between tasks. To start, allocate independent tasks to
  team members.
- Let your team know when a task or piece of functionality is complete.
- Discuss frequently.

At the implementation level:

- Use a version control system, such as Git

  - Nothing that is committed can get lost with version control - your
    teammate cannot accidentally delete your code.
  - Commit changes frequently and in small chunks. This makes clear to
    others what you are working on, and if there are any conflicts
    small changes make these easier to resolve.
  - It makes it easy to switch between computers.

- Add tests for the functionality that use develop. This:

  - Builds confidence that your implementation is correct

  - Can detect if a change by another team member has affected your
    implementations. (One of the most frustrating situations in team
    development is when a change by another team members breaks your
    carefully constructed functionality.)


Using Git
---------

`Git <https://git-scm.com/>`_ is modern and the most popular *version
control system* (VCS). A version control system tracks changes to
source code.  It can show what has changed, and who has made changes.
For example, Android development uses git and you can see all the code
changes to the public version of Android at
https://github.com/android.

Key steps:


Use the `help forum
<https://www.allanswered.com/community/148/cued-part-ia-computing/>`_ to get help.

Tutorials:

- https://try.github.io
- https://swcarpentry.github.io/git-novice/
- https://www.atlassian.com/git/tutorials/


How often should I commit changes?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Often. Structure your work into small chunks, and commit after
completing each 'chunk'. At the very least, you should commit changes
at the completion of each *Task* in the *Deliverables* section.
