Getting started
===============

You are required to use Git in this activity.  It is strongly
recommended that you use a hosted Git service at `Bitbucket
<https://bitbucket.org/>`__, `GitHub <https://github.com/>`_, or
`GitLab <https://about.gitlab.com/>`_.

If you are unfamiliar with these services, use `Bitbucket
<https://bitbucket.org/>`__. The following instructions are for
Bitbucket and novice users. If you are experiences Git user you will
likely have your own workflow.

#. Create an account on Bitbucket and login. Share your username with
   your project partner.

#. One team member should create a copy ('fork') of the starter code
   by going to::

     https://bitbucket.org/garth-wells/cued-partia-lent-activity/fork

   a. Tick the box 'This is a private repository'.

   #. From the overview page
      (https://bitbucket.org/dashboard/overview) you should see your
      repository. Click on it.

   #. On your repository page, click 'Share' and enter the username of
      your project partner give them 'write' or 'admin' access.

#. To 'clone' your repository from the menu on the left-hands side of
   the repository page, click 'Clone' and copy the command. With this
   command your can clone a copy of the repository to your computer, e.g.::

     git clone https://bitbucket.org/john-doe/cued-partia-lent-activity.git

   You should now have a local (on your computer) copy of the code.

#. Enter the code directory attempt to execute file ``Task1A.py``::

     python Task1A.py

   or::

     python3 Task1A.py

.. todo::

   More getting started.

.. note::

  You may need, at times, to review the activity notebooks for
  Michaelmas Term.
